<%@ page import="pl.sda.library.Author" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Authors</title>
</head>
<body>

<c:choose>
    <c:when test="${param.action eq 'add'}">
        <% addCategory(request, session); %>
    </c:when>
    <c:when test="${param.action eq 'edit'}">
        <% editCategory(request, session); %>
    </c:when>
    <c:when test="${param.action eq 'remove'}">
        <% removeCategory(request, session); %>
    </c:when>
</c:choose>

<c:choose>
    <c:when test="${param.view eq 'list'}">
        <jsp:forward page="author/author_list.jsp"></jsp:forward>
    </c:when>
    <c:when test="${param.view eq 'add_form'}">
        <jsp:forward page="author/author_add_form.jsp"></jsp:forward>
    </c:when>
    <c:when test="${param.view eq 'edit_form'}">
        <jsp:forward page="author/author_edit_form.jsp"></jsp:forward>
    </c:when>
    <c:otherwise>
        <jsp:forward page="author/author_list.jsp"></jsp:forward>
    </c:otherwise>
</c:choose>

</body>
</html>
<%!
    private void addCategory(HttpServletRequest request, HttpSession session) {
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        Author author = new Author(name, surname);
        Map<Integer, Author> authors = (Map<Integer, Author>) session.getAttribute("authors");

        if (authors == null) {
            authors = new HashMap<>();
        }
        authors.put(author.getId(), author);
        session.setAttribute("authors", authors);
    }

    private void editCategory(HttpServletRequest request, HttpSession session) {
        int id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        Map<Integer, Author> authors = (Map<Integer, Author>) session.getAttribute("authors");

        if (authors == null) {
            authors = new HashMap<>();
        }

        Author author = authors.get(id);
        author.setName(name);
        session.setAttribute("authors", authors);
    }

    private void removeCategory(HttpServletRequest request, HttpSession session) {
        int id = Integer.parseInt(request.getParameter("id"));
        Map<Integer, Author> authors = (Map<Integer, Author>) session.getAttribute("authors");

        if (authors == null) {
            authors = new HashMap<>();
        }

        Author author = authors.remove(id);
        session.setAttribute("authors", authors);
    }
%>