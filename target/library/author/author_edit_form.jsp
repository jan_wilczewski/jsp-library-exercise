<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
    <title>Edycja autora</title>
</head>
<body>
<%@include file="../common/menu.jspf"%>
<fieldset>
<legend>Edycja autora</legend>
<form action="<c:url value="../author.jsp"/>" method="post">
    <input type="hidden" name="action" value="edit" />
    <input type="hidden" name="id" value="${param.id}" />

    Zmień imię na: <input type="text" name="name" value="${categories[Integer.valueOf(param.id)].name}" /><br />
    Zmień nazwisko na: <input type="text" name="surname" value="${categories[Integer.valueOf(param.id)].surname}" /><br />

    <input type="submit" name="submit" value="edytuj" />
</form>
</fieldset>
</body>
</html>
